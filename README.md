# Bike-Share Case Study

For the final of my Google Data Analytics course, I was tasked using the last 12 months of bike-shares ride data to produce insights into the key differences between casual riders and members. 

The complete Tableau visualization can be found [here](https://public.tableau.com/app/profile/dylan.jl/viz/Bike-ShareCaseStudy/Story1).

My blog detailing the process can be found [here](https://dylanjackson.gitlab.io/dylanjackson/writings/)

## Contents:

- modified_data: Contains any files that were derived from the original data.
- trip_data: Raw csv files of each months data, as well as, tripData.csv all records in one file but unmodified otherwise.
- bike-share-notebook: the html file to view the notebook as well as the R Notebook to compile and run for yourself

tripdata.csv in the modified_data folder is the final, cleaned data which was used in visualization.