---
title: "Bike-Share Case Study R Notebook"
output: html_notebook
---

This Notebook catalogs the R related work done to complete the Capstone Case Study for [Googles Data Analytics Course](https://www.coursera.org/professional-certificates/google-data-analytics).

Essentially, given the previous 12 months of ride information, find insights into how casual users and members differ.

Throughout the notebook their will be data read which was modified externally. Links will be provided to [my blog](https://dylanjackson.gitlab.io/dylanjackson/writings/bike-share-1/) which details how this data was obtained.

```{r adding dependacies, eval=FALSE, include=FALSE}
install.packages("tidyverse")
```

```{r include=FALSE}
library(tidyverse)
library(lubridate)
```

```{r reading in the file, eval=FALSE, include=FALSE}
# Data set containing ride information from April, 2020 to June, 2021
tripData <- read.csv(file = "./trip_data/tripdata.csv",fill = TRUE,na.strings=c("","NA"))

# Rides which have duplicated ride_id's as a result of an internal bug.
# For more information on these duplicated values visit https://dylanjackson.gitlab.io/dylanjackson/writings/bike-share-1/
duplicate_values <- read.csv(file = "./modified_data/duplicate_values.csv",
                             fill = TRUE, na.strings=c("","NA"))
```

# Removing Duplicate Rows and Obtaining Unique Station Names

```{r removing duplicate entries}
# Removing the duplicated entries from tripData
tripData <- filter(tripData,
  !(tripData$ride_id %in% duplicate_values$ride_id &
  tripData$started_at %in% duplicate_values$started_at))
```

```{r "Removing Start and End Station Id columns"}
# Stations already have unique names, they are more description so we will not need these Ids.
tripData <- tripData %>% 
  select(-c(start_station_id,end_station_id))
```

```{r getting unique station names}
# Obtaining a list of unique station names.
# This information will be processed in excel to fix any errors.
# https://dylanjackson.gitlab.io/dylanjackson/writings/bike-share-2/

start_names <- tripData %>% 
  select(start_station_name) %>% 
  rename(station_name = start_station_name)

end_names <- tripData %>% 
  select(end_station_name) %>% 
  rename(station_name = end_station_name)

station_names <-
  rbind(start_names,end_names) %>% 
  drop_na() %>%
  unique()
```

```{r eval=FALSE, include=FALSE}
write_csv(station_names, "./modified_data/station_names.csv")
```

# Times, Dates, and Weekdays

```{r}
# Coverting dates from character to POSIXct
tripData$started_at <- as_datetime(tripData$started_at, tz="America/Chicago")
tripData$ended_at <- as_datetime(tripData$ended_at, tz="America/Chicago")

# Removing rides which ended before they began.
tripData <- tripData %>% 
  filter(started_at < ended_at)

# Obtaining the trip duration in minutes for each entry.
tripData$trip_duration <- 
  as.double(difftime(tripData$ended_at,
                   tripData$started_at,
                   units = "mins"))

# Obtaining the weekday for each entry
tripData$weekday <- weekdays(tripData$started_at)
```

# Fixing station names

```{r loading fixed station names}
# These are the raw station names and their associated fixed names.
# This was manually cleaned in excel, for information on this process visit this link.
# https://dylanjackson.gitlab.io/dylanjackson/writings/bike-share-2/
fixed_station_names <- read_csv("./modified_data/fixed_station_names.csv")
```

```{r}
# Combining start and end stations into a single column, along with their associated longitude and latitude.
start_data <- tripData %>% 
  select(start_station_name, start_lat, start_lng) %>% 
  rename(station_name = start_station_name, lng = start_lng, lat = start_lat)

end_data <- tripData %>% 
  select(end_station_name, end_lat, end_lng) %>% 
  rename(station_name = end_station_name, lng = end_lng, lat = end_lat)

station_data <- start_data %>% 
  rbind(end_data) %>% 
  drop_na()

# Joining the combined stations and coordinates with their associated corrected name
station_data <- 
  inner_join(station_data, fixed_station_names)

# Using median to obtain a set of uniform coordinates for each station
station_data_medians <-station_data %>% 
  group_by(actual_name) %>% 
  summarize(median_lat = median(lat), median_lng = median(lng))
```

```{r plotting lat and lng for each street}
# Quick check to see how the last step turned out.
station_data_medians %>%
ggplot(aes(x = median_lat, y = median_lng)) +
  geom_point()
```

```{r writing station_data_medians, eval=FALSE, include=FALSE}
write.csv(station_data_medians, "./modified_data/station_data_medians.csv")
```

# No Null Stations, Proper Names and Coordinates

```{r "Create dataframe with proper names, coordinates, and the other stuff"}
# # Join fixed station names with median coordinates
names_coords <- fixed_station_names %>% 
  inner_join(station_data_medians)
summary(names_coords)

# Remove old coordinates from main data set
data_no_coords <- tripData %>% 
  select(-c("start_lat","start_lng","end_lat","end_lng"))

# Rename data frame columns for start station name merging
start_names_coords <- names_coords %>% 
  rename( start_name = actual_name,
          start_lat = median_lat,
          start_lng = median_lng)
# Replacing old start station names and coordinates with the new ones.
tripData_with_good_start <- data_no_coords %>% 
  inner_join(start_names_coords, by = c("start_station_name" = "station_name"))

# Rename data frame columns for start station name merging
end_names_coords <- names_coords %>% 
  rename( end_name = actual_name,
          end_lat = median_lat,
          end_lng = median_lng)

# Replacing old end station names and coordinates with the new ones
tripData <- tripData_with_good_start %>% 
  inner_join(end_names_coords, by = c("end_station_name" = "station_name")) %>% 
  select(-c("start_station_name","end_station_name"))No Null Stations, Proper Names and Coordinates
```
